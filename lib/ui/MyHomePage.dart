import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String appName = "";
  String packageName = "";
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    getInfo();
  }

  getInfo() async {
    isLoading = true;
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    appName = packageInfo.appName;
    packageName = packageInfo.packageName;
    isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: isLoading ? Text("default") : Text(appName),
      ),
      body: isLoading
          ? Center(child: Text("Loading bro. . ."))
          : Center(child: Text(packageName)),
    );
  }
}
